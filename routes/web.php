<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PagesController@welcome');
Route::get('/register','PagesController@register');
Route::get('/login','PagesController@login');
Route::get('/user','PagesController@user');
Route::get('/item','PagesController@item');
Route::get('/additem','PagesController@additem');

Route::post('/store','UserController@store');
Route::post('/into','UserController@into');
