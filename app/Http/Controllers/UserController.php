<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

?>

<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>

<?php

class UserController extends Controller
{
    public function store(request $request) {
    	$name=$request->input('name');
    	$email=$request->input('email');
    	$phone=$request->input('phone');
    	$password=$request->input('password');

    	$data = array('name'=>$name,'email'=>$email,'phone'=>$phone,'password'=>$password);
        foreach($data as $d) {
            if($d == '') {
                return view('register');
            }
        else {

    	DB::table('user')->insert($data);
    	?>

    	<h6 align="center"> 

    		<?php
    		echo "User registered successfully.";
    		?>

    	</h6>
    	
    	<?php
    	return view('login');
    } } }

    public function into(request $request) {
    	$name=$request->input('name');
    	$gender=$request->input('gender');
    	$phone=$request->input('phone');
    	$email=$request->input('email');
    	$age=$request->input('age');
    	$item=$request->input('item');

    	$data = array('name'=>$name,'gender'=>$gender,'phone'=>$phone,'email'=>$email,'age'=>$age,'item'=>$item);

    	if($age<15 || $age>45) { 
    		?>
    		
    		<h4 align="center">
    		<?php echo "Customer age must be 15-45"; ?>
    		</h4>
    		
    		<?php
    	} else {

    	DB::table('customer')->insert($data);
    	?>

    	<h4 align="center"> 

    		<?php
    		echo "Successfully registered as customer.<br/><br/>";
    		echo "<br/><br/> Your username with <b><i>$name</b></i> and selected item is <b><i>$item</b></i>.<br/><br/>";
    		?>

    	</h4></body></html>
    	
    	<?php
    }}
}
?>