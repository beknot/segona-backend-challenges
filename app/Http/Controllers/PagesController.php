<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function welcome() {
    	return view('welcome');
    }
    public function register() {
    	return view('register');
    }
    public function login() {
    	return view('login');
    }
    public function item() {
    	return view('item');
    }
    public function additem() {
        return view('additem');
    }
    public function user() {
    	return view('user');
    }
}
?>