@extends('layout.app')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>@yield('title','Register')</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>

<body>
    <div class="jumbotron">
        <div class="container" style="height:1px">
        <h3 align="center">Register</h3>
    </div></div>

    <form class="form-group" method="post" action="/store">
        <table width="320px" align="center" class="table-hover" >
        <tbody>
            <tr>
                {{ csrf_field() }}
                <td><label for="name">Name</label></td>
                }
            <td><input type="text" name="name" placeholder="name"/></td>
            </tr>
            <tr><td><label for="email">Email</label></td>
            <td><input type="email" name="email" placeholder="email"/></td>
            </tr>
            <tr><td><label for="phone">Phone</label></td>
            <td><input type="text" name="phone" placeholder="phone"/></td>
            </tr>
            <tr><td><label for="password">Password</label></td>
            <td><input type="password" name="password" placeholder="*******"/></td>
            </tr>
            <tr><td><label for="c_password">Confirm password</label></td>
            <td><input type="password" name="c_password" placeholder="*******" /></td>
            </tr>
            <tr><td></td><td><input type="submit" name="submit" class="btn btn-primary" value="Register"></td>
            </tr>
            </tbody>
        </table>
    </form>
</body>
</html>
