@extends('layout.app')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>@yield('title','Add Item')</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>

<body>
    <form class="form-group" method="POST" action="#">
        
        <table width="400px" align="center" class="table-hover" >
        <tbody>
            <tr><td><label for="name">Name</label></td>
            <td><input type="text" placeholder="name"/></td>
            </tr>
            <tr><td><label for="description">Description</label></td>
            <td><input type="text" placeholder="description"/></td>
            </tr>
            <tr><td><label for="created">Created date</label></td>
            <td><input type="date" placeholder="created date"/></td>
            </tr>
            <tr><td><label for="photo">Created date</label></td>
            <td><input type="file"/></td>
            </tr>
            <tr><td></td><td><button type="button" class="btn btn-primary" onclick="location.href='{{ url('item') }}'">Register</button></td>
            </tr>
            </tbody>
        </table>
        <!--<table width="320px" align="center" class="table-hover" >
        <tbody>
            <tr><td>Name</td>
            <td><input type="text" placeohlder="name"/></td>
            </tr>
            <tr><td>Email</td>
            <td><input type="email" placeohlder="name"/></td>
            </tr>
            <tr><td>Password</td>
            <td><input type="password" placeohlder="name"/></td>
            </tr>
            <tr><td>Confirm password</td>
            <td><input type="password"/></td>
            </tr>
            <tr><td></td><td><button type="button" class="btn btn-primary">Register</button></td>
            </tr>
            </tbody>
        </table>-->
    </form>
</body>
</html>
