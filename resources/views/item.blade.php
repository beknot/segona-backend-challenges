@extends('layout.app')
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>@yield('title','Item')</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body>
}


    <div class="jumbotron">
        <div class="container" style="height:5px">
        <h3>Item</h3>
        <div class="search-box" style="float:right">
            <input type="text" placeholder="search">
            <button type="button" class="btn btn-info">
            <span class="glyphicon glyphicon-search"></span> Search</button>
        </div>
        <div class="add-item">
            <button type="button" class="btn btn-info" onclick="location.href='{{ url('additem') }}'"
            >
            <span class="glyphicon glyphicon-plus"></span>Add item</button>
        </div>
    </div></div>
</body>
</html>