@extends('layout.app')
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>@yield('title','Login')</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>

<body>
    <div class="jumbotron">
        <div class="container" style="height:1px">
        <h3 align="center">Login</h3>
    </div></div>


    <form class="form-group" method="POST" action="#">
        <table width="320px" align="center" class="table-hover" >
        <tbody>  {{ csrf_field() }}
            <tr><td><label for="email">Email</label></td>
            <td><input type="email" placeholder="email"/></td>
            </tr>
            <tr><td><label for="password">Password</label></td>
            <td><input type="password" placeholder="********"/></td>
            </tr>
            <tr><td></td><td><input type="submit" class="btn btn-primary" value="Login"></td>
            </tr>
            </tbody>
        </table>
    </form>
</body>
</html>
