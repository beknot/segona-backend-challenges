@extends('layout.app')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>@yield('title','User')</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>

<body>
    <form class="form-group" method="POST" action="/into">
        <table width="320px" align="center" class="table-hover" style="margin-top:60px">
        <tbody>
            <h4 align="center" style="margin-top:10px">Register customer</h4>
            {{ csrf_field() }}
            <tr><td><label for="name">Name</label></td>
            <td><input type="text" name="name" placeholder="name"/></td>
            </tr>
            <tr><td><label for="gender">Gender</label></td>
            <td><input type="text" name="gender" placeholder="gender"/></td>
            </tr>
            <tr><td><label for="phone">Phone</label></td>
            <td><input type="text" name="phone" placeholder="phone"/></td>
            </tr><tr><td><label for="email">Email</label></td>
            <td><input type="text" name="email" placeholder="email"/></td>
            </tr>
            <tr><td><label for="age">Age</label></td>
            <td><input type="text" name="age" placeholder="age"/></td>
            </tr>
            <tr><td><label for="item">Item</label></td>
            <td><select name="item" >
                <option>Laptop</option><option>CPU</option>
                <option>Monitor</option><option>Keyboard</option>
                <option>Mouse</option><option>Printer</option>
                <option>RAM</option><option>Harddisk</option>
                <option>USB drive</option>
            </select></td>
            </tr>
            <tr><td></td><td><input type="submit" class="btn btn-primary"value="Register"></td>
            </tr>
            </tbody>
        </table>
    </form>
</body>
</html>
